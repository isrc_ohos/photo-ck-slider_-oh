CkSlider_ohos
========

**本项目是基于开源项目 CkSlider进行鸿蒙化的移植和开发的，并适配openharmony3.0版本，通过项目标签以及github地址（ https://github.com/tudousi/ck_slide ）追踪到原安卓项目版本**

#### 项目介绍

- 项目名称：图片轮播组件

- 所属系列：鸿蒙的第三方JS组件适配移植

- 功能：

     ​    完成openharmony适配版本。

     ​    完成组件的图片导入。
     ​    完成组件轮播图的UI移植
     ​    完成组件的轮播效果移植。
     ​    同时可以左右进行图片切换轮播效果
     ​    可以根据按钮选择上一张图、下一张图、也可以直接跳到最后一张图，同时拥有渐变的效果，也就是淡入淡出效果。

- 项目移植状态：主功能

- 调用差异：无

- 开发版本：sdk5，DevEco Studio2.1 beta3

- 项目发起作者：张馨心

- 邮箱：1061053450@qq.com

- 原项目Doc地址：https://github.com/tudousi/ck_slide

#### 项目介绍

- 编程语言：JS 

JS图片轮播组件


#### 安装教程

1. 下载CkSlider的har包CkSlider.har（位于output文件夹下）。
2. 启动 DevEco Studio，将下载的har包，导入工程目录“entry->libs”下。

3. 在moudle级别下的build.gradle文件中添加依赖，在dependences标签中增加对libs目录下jar包的引用。

```
dependencies {
    implementation fileTree(dir: 'libs', include: ['*.jar', '*.har'])
	……
}
```

4. 在导入的har包上点击右键，选择“Add as Library”对包进行引用，选择需要引用的模块，并点击“OK”即引用成功。

在sdk5，DevEco Studio2.1 beta3下项目可直接运行
如无法运行，删除项目.gradle,.idea,build,gradle,build.gradle文件
并依据自己的版本创建新项目，将新项目的对应文件复制到根目录下

#### 使用说明

1.CkSlider是一种基于鸿蒙JS的的实现播放多个广告图片和手动滑动循环等功能的界面，功能已满足大部分要求。本组件有部分方法调用鸿蒙原生方法体。

2.使用方法

```
    //最后一张
    swipeTo() {
        this.$element('swiper').swipeTo({index: 2});
    },
    //下一张
    showNext() {
        this.$element('swiper').showNext();
    },
    //上一张
    showPrevious() {
        this.$element('swiper').showPrevious();
    },
    onload: function(data) {
        this.width = data.width;
        this.height = data.height;
    }

```

#### 效果演示

![image-20220418163424902](C:/Users/zhangxinxin/AppData/Roaming/Typora/typora-user-images/image-20220418163424902.png)




#### 版权和许可信息

CkSlider_ohos经过[Apache License, version 2.0](http://www.apache.org/licenses/LICENSE-2.0)授权许可.

